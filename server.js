const Net = require("net");
const express = require("express");
const socketio = require("socket.io");
const dayjs = require("dayjs");

const fidsPort = process.env.FIDS_PORT || 6000;
const servicePort = process.env.SERVICE_PORT || 6002;

const app = express();

app.use(express.static(__dirname + "/public"));

const appServer = app.listen(servicePort);
const io = socketio(appServer);

io.on("connection", function(socket) {
  console.log("connection received from client");
});

const server = new Net.Server();

server.listen(fidsPort, function() {
  console.log(`Server listening for connection requests on socket ${fidsPort}`);
});

server.on("connection", function(socket) {
  socket.on("data", function(chunk) {
    const data = chunk.toString().trim();
    console.log(`Data received from client: ${data}`);

    if (data.toLowerCase() != "heartbeat") {
      const body = data.split("_");
      if(body[3].toLowerCase() == "alarm") {
        const fidsAlarm = {
          stationId: body[0],
          zoneId: body[1],
          alarmId: body[2],
          date: new dayjs().format("YYYY-MM-DDTHH:mm:ss")
        };

        io.emit("fidsAlarm", fidsAlarm);
      }
    } else {
      io.emit("fidsAlarm", data)
    };
  });

  socket.on("end", function() {
    console.log("Closing connection with the client");
  });

  socket.on("error", function(err) {
    console.log(`Error: ${err}`);
  });
});
